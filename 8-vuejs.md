# Apache Cordova Framework - Vue.js

## Vue.js

Vue (pronunciado /vjuː/, como view) es un framework progresivo para construir interfaces de usuarios. A diferencia de otros frameworks monoliticos, Vue está diseñado desde cero para ser incremental adaptable. La librería core está enfocado sólo en la capa de vista, y es muy fácil entender e integrar con otras librerías o proyectos existentes. Por otro lado, Vue es perfectamente capaz de alimentar sofisticadas aplicaciones de una sola página cuando se utiliza en combinación con herramientas modernas y librerías de apoyo.
Si usted es un desarrollador de frontend con experiencia y desea saber cómo Vue se compara con otros frameworks, echa un vistazo a la comparación con otros frameworks.

[Vue.js Sitio Oficial](https://vuejs.org)

## (Des) Configuración (Desarrollo)

Para realizar las pruebas vamos a eliminar el tag de CSP (Content Security Policy) ubicado en el header.

```
<meta http-equiv="Content-Security-Policy" content="default-src ...">
```

## Instalación

### 1. Agregamos la librería de Vue.

```
<script src="https://vuejs.org/js/vue.js"></script>
```

### 2. Agregamos un atributo id en el DOM y un una variable message con mustache syntax

```
<div class="app" id="app">
  <h1>Apache Cordova {{message}}</h1>
</div>
```
### 3. Agregamos una función para inicializar el principar componente Vue

```
<script type="text/javascript">
function initVue() {
  var app = new Vue({
    el: '#app',
    data: {
      message: ' + Vue.js'
    }
  });
}
</script>
```

### 4. Agregamos la función en el evento deviceready

```
document.addEventListener("deviceready", initVue, false);
```

### 5. Corremos la app

```
cordova run browser
```

## Versionado

### 1. Instalar Vue con npm

```
npm i vue --save
```

### 2. Agregar script para copiar los recursos

```
{
  "scripts": {
    "copy-vue-dev": "cp ./node_modules/vue/dist/vue.js ./www/js/",
    "copy-vue-prod": "cp ./node_modules/vue/dist/vue.min.js ./www/js/",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```

### 3. Reemplazar referencia a librería

```
//Librería para desarrollo
<script type="text/javascript" src="js/vue.js"></script>
//Librería mimificada para produccion
/*<script type="text/javascript" src="js/vue.min.js"></script>*/
```

### 4. Corremos el script

```
npm run copy-vue-dev
```

### 5. Corremos la app

## Código

### 1. Agregamos 3 views

```
// www/js/index.js
var viewApache = {
  title: 'Apache Cordova Framework'
};
var viewVue = {
  title: 'Vue.js Framework'
};
var viewMobile= {
  title: 'Mobile Development'
};

var app = new Vue({
  el: '#app',
  data: {
    viewApache: viewApache,
    viewVue: viewVue,
    viewMobile: viewMobile
  }
});
```

```
<!-- www/index.html -->
<div id="app">
  <div>
    <h1>{{viewApache.title}}</h1>
  </div>
  <div>
    <h1>{{viewVue.title}}</h1>
  </div>
  <div>
    <h1>{{viewMobile.title}}</h1>
  </div>
</div>
```

### 2. Agregamos view actual

```
// www/js/index.js
var app = new Vue({
  el: '#app',
  data: {
    currentView: viewApache,

    viewApache: viewApache,
    viewVue: viewVue,
    viewMobile: viewMobile
  }
});
```

```
<!-- www/index.html -->
<div id="app">
  <div v-show="currentView === viewApache">
    <h1>{{currentView.title}}</h1>
  </div>
  <div v-show="currentView === viewVue">
    <h1>{{currentView.title}}</h1>
  </div>
  <div v-show="currentView === viewMobile">
    <h1>{{currentView.title}}</h1>
  </div>
</div>
```

### 3. Agregamos botones y eventos

```
// www/js/index.js
var nextView = function(view) {
  switch (view) {
    case viewApache: this.currentView = viewVue; break;
    case viewVue: this.currentView = viewMobile; break;
  }
  console.log('next view', this.currentView.title);
};
var prevView = function(view) {
  switch (view) {
    case viewVue: this.currentView = viewApache; break;
    case viewMobile: this.currentView = viewVue; break;
  }
  console.log('prev view', this.currentView.title);
};

var app = new Vue({
  el: '#app',
  data: {
    currentView: viewApache,
    viewApache: viewApache,
    viewVue: viewVue,
    viewMobile: viewMobile
  },
  methods: {
    next: nextView,
    prev: prevView
  }
});
```

```
<!-- www/index.html -->
<div id="app">
  <div v-show="currentView === viewApache">
    <h1>{{currentView.title}}</h1>
    <button v-on:click="next(currentView)">Next</button>
  </div>
  <div v-show="currentView === viewVue">
    <h1>{{currentView.title}}</h1>
    <button v-on:click="prev(currentView)">Back</button>
    <button v-on:click="next(currentView)">Next</button>
  </div>
  <div v-show="currentView === viewMobile">
    <h1>{{currentView.title}}</h1>
    <button v-on:click="prev(currentView)">Back</button>
  </div>
</div>
```

### 4. Agregamos estados en el historial

```
var viewApache = {
  name: 'apache',
  title: 'Apache Cordova Framework'
};
var viewVue = {
  name: 'vue',
  title: 'Vue.js Framework'
};
var viewMobile= {
  name: 'mobile',
  title: 'Mobile Development'
};
var nextView = function(view) {
  switch (view) {
    case viewApache: this.currentView = viewVue; break;
    case viewVue: this.currentView = viewMobile; break;
  }
  console.log('next view', this.currentView.title);
  history.pushState({}, this.currentView.title, this.currentView.name);
};
var prevView = function(view) {
  switch (view) {
    case viewVue: this.currentView = viewApache; break;
    case viewMobile: this.currentView = viewVue; break;
  }
  console.log('prev view', this.currentView.title);
  history.replaceState({}, this.currentView.title, this.currentView.name);
}
```

## Routing

### 1. Instalamos Vue-Route con npm

```
npm i vue-router --save
```

### 2. Agregamos el script para copiar el recurso

```
{
  "scripts": {
    ...
    "copy-vue-router": "cp ./node_modules/vue-router/dist/vue-router.js ./www/js/",
    ...
  }
}
```

### 3. Agregamos la librería de Vue-Router

```
<script type="text/javascript" src="js/vue-router.js"></script>
```
### 4. Cambiamos el contenido html

```
<!-- www/index.html -->
<div id="app">
  <router-link to="/apache" tag="button">Go to Apache</router-link>
  <router-link to="/vue" tag="button">Go to Vue</router-link>
  <router-link to="/mobile" tag="button">Go to Mobile</router-link>
  <router-view></router-view>
</div>
```

### 5. Agregamos los templates para los contenidos

```
<!-- www/index.html -->
<div id="app">
  ...
</div>

<script type="text/x-template" id="view-apache">
  <h1>{{ message }}</h1>
</script>
<script type="text/x-template" id="view-vue">
  <h1>{{ message }}</h1>
</script>
<script type="text/x-template" id="view-mobile">
  <h1>{{ message }}</h1>
</script>
```

### 6. Creamos los componentes para los templates

```
var viewApache = Vue.component('view-apache', {
  template: '#view-apache',
  data: function () {
    return { message: 'Apache Cordova Framework' }
  }
});
var viewVue = Vue.component('view-vue', {
  template: '#view-vue',
  data: function () {
    return { message: 'Vue.js Framework' }
  }
});
var viewMobile = Vue.component('view-mobile', {
  template: '#view-mobile',
  data: function () {
    return { message: 'Mobile Development' }
  }
});
```

### 7. Definimos el router

```
var router = new VueRouter({
  routes: [
    { path: '/apache', component: viewApache },
    { path: '/vue', component: viewVue },
    { path: '/mobile', component: viewMobile }
  ]
});
var vm = new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname
  },
  router: router
});
```

### 8. Corremos la app
