# Apache Cordova Framework - Bootstrap

## Bootstrap
Bootstrap es un framework o conjunto de herramientas de Código abierto para diseño de sitios y aplicaciones web. Contiene plantillas de diseño con tipografía, formularios, botones, cuadros, menús de navegación y otros elementos de diseño basado en HTML y CSS, así como, extensiones de JavaScript opcionales adicionales.

[Bootstrap Sitio Oficial](https://getbootstrap.com/docs/3.3/)

[Bootstrap GitHub](https://github.com/twbs/bootstrap)

## Preparación de entorno

### 1. Creamos un proyecto cordova

```
cordova create cordova-bootstrap
```

### 2. Agregamos al proyecto soporte para las plataformas `browser` y `android`

```
cordova platform add android
cordova platform add browser
```

### 3. Verificamos que el proyecto tiene el archivo package.json, si no lo tiene lo creamos

```
npm init
```

## (Des) Configuración (Desarrollo)

Para realizar las pruebas vamos a eliminar el tag de CSP (Content Security Policy) ubicado en el header.

```
<meta http-equiv="Content-Security-Policy" content="default-src ...">
```

## Instalación

### 1. Agregamos las librerías de Bootstrap.

```
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
```

### 2. Eliminamos las referencias de index.css y index.js

```
<!-- <link rel="stylesheet" type="text/css" href="css/index.css"> -->

<!-- <script type="text/javascript" src="js/index.js"></script> -->

```
### 3. Agregamos un pequeño bloque para probar la correcta instalación

```
<div class="container">
  <div class="row text-center">
    <h1>Bootstrap Example</h1>
  </div>
</div>
```

### 4. Corremos la app

```
cordova run browser
```

## Versionado

### 1. Instalamos Bootstrap y Jquery con npm

```
npm i bootstrap@3.3.7 --save
npm i jquery@1.11.3 --save
```

### 2. Agregar script para copiar los recursos

Linux
```
{
  "scripts": {
    "cp-bt-js": "cp ./node_modules/bootstrap/dist/js/*.min.js ./www/js/",
    "cp-bt-css": "cp ./node_modules/bootstrap/dist/css/*.min.css ./www/css/",
    "cp-bt-fonts": "cp -r ./node_modules/bootstrap/dist/fonts ./www/",
    "cp-jquery": "cp -r ./node_modules/jquery/dist/*.min.js ./www/js",
    "cp-bootstrap": "npm run cp-bt-js && npm run cp-bt-css && npm run cp-bt-fonts && npm run cp-jquery",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```

Windows
```
{
  "scripts": {
    "cp-bt-js": "copy .\\node_modules\\bootstrap\\dist\\js\\*.min.js .\\www\\js\\",
    "cp-bt-css": "copy .\\node_modules\\bootstrap\\dist\\css\\*.min.css .\\www\\css\\",
    "cp-bt-fonts": "copy .\\node_modules\\bootstrap\\dist\\fonts .\\www\\",
    "cp-jquery": "copy .\\node_modules\\jquery\\dist\\*.min.js .\\www\\js",
    "cp-bootstrap": "npm run cp-bt-js && npm run cp-bt-css && npm run cp-bt-fonts && npm run cp-jquery",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```
### 4. Corremos el script

```
npm run cp-bootstrap
```

### 5. Reemplazar referencia a librería

```
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
```

### 6. Corremos la app

## Componentes

### 1. Agregamos una barra de navegación

```
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed"
          data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
      </button>
      <a class="navbar-brand" href="#">Bootstrap</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse">
      <p class="navbar-text navbar-right">Curso Apache Cordova</p>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
```

[NavBar Documentación](https://getbootstrap.com/docs/3.3/components/#navbar)

### 2. Agregamos un panel

```
<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">Titulo</div>
    <div class="panel-body">
      Contenido
    </div>
  </div>
</div>
```

[Panels Documentación](https://getbootstrap.com/docs/3.3/components/#panels)

### 3. Agregamos un formulario

```
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Check me out
    </label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
```

[Forms Documentación](https://getbootstrap.com/docs/3.3/css/#forms)

### 4. Agregamos un Grid System

```
<div class="container">
  <!-- Stack the columns on mobile by making one full-width and the other half-width -->
  <div class="row">
    <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  </div>

  <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
  <div class="row">
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  </div>

  <!-- Columns are always 50% wide, on mobile and desktop -->
  <div class="row">
    <div class="col-xs-6">.col-xs-6</div>
    <div class="col-xs-6">.col-xs-6</div>
  </div>
</div>
```

[Grid Documentación](https://getbootstrap.com/docs/3.3/css/#grid)
[Grid Medidas](https://getbootstrap.com/docs/3.3/css/#grid-options)

## Ejercicios
Utilizando **Bootstrap Framework** trataremos de acercarnos al wireframe presentado en clase.
