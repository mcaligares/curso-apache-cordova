# Apache Cordova Framework - Eventos

## Eventos

Cordova nos provee una serie de eventos para poder utilizarlos dentro de una aplicación.

### deviceready

El evento `deviceready` es disparado cuando Cordova es completamente cargado. Éste evento es esencial para cualquier aplicación. Esta es la señal de que las _device API's de cordova_ están cargadas y lista para ser usadas.

Cordova consiste de dos códigos bases: Nativo y JavaScript. Mientras el codigo nativo se carga, se muestra una imagen de cargando customizada. De cualquier modo, JavaScript se va cargando a medida que se carga el DOM. Esto quiere decir que la aplicación potencialmente puede llamar a la función JavaScript de Cordova antes de que el código nativo esté disponible.

El evento `deviceready` es disparado cuando Cordova es completamente cargado. Una vez el evento es disparado, puedes hacer llamadas a la API de Cordova sin problemas. Normalmente las aplicaciones agregan un _event listener_ con `document.addEventListener` una vez que el DOM del HTML ha sido cargado.

```
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    // Now safe to use device APIs
}
```

### pause

El evento `pause` se dispara cuando la plataforma nativa pone la aplicación en segundo plano _(background)_, normalmente cuando el usuario cambia a una aplicación diferente.

```
document.addEventListener("pause", onPause, false);

function onPause() {
    // Handle the pause event
}
```
### resume

El evento `resume` se dispara cuando la plataforma nativa pone la aplicación en primer plano.

```
document.addEventListener("resume", onResume, false);

function onResume() {
    // Handle the resume event
}
```

[Más Info de Eventos](https://cordova.apache.org/docs/en/latest/cordova/events/events.html)

## Ejercicios

1. Crear una apliación cordova llamada **cordova-eventos**
2. Eliminar el elemento _meta Content-Security-Policy_
3. Agregar un log/alert al momento de cargar el documento index.html. Documentación: [w3schools onload Event](https://www.w3schools.com/jsref/event_onload.asp),
  [Mozilla Global Event Handler onload](https://developer.mozilla.org/es/docs/Web/API/GlobalEventHandlers/onload)
4. Agregar un log/alert en el _eventListener_ de los eventos **pause** y **resume**.
5. Probar la aplicación en un dispositivo para ver los resultados.
