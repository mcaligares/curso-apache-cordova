# Apache Cordova Framework
## Introducción

Apache Cordova es un framework de desarrollo móvil de código abierto. Permite usar tecnologías WEB como HTML5, CSS3 y JavaScript para un desarrollo multiplataforma. Las aplicaciones se ejecutan en un wrapper para cada plataforma y utiliza una API compatible con estándares para acceder a las capacidades de cada dispositivo, como sensores, datos, estado de la red, etc.

_Apache Cordova no proporciona UI widgets ni MV\* frameworks. Cordova proporciona sólo la estructura en el que pueden ejecutarse. Si desea utilizar UI widgets y/o un framework MV\*, deberá seleccionarlos e incluirlos en su aplicación._

### Arquitectura

![alt text](https://cordova.apache.org/static/img/guide/cordovaapparchitecture.png "Arquitectura de Aplicacion Cordova")

### WebApp

Es una página o un conjunto de páginas con contenido dinámico o estático que está orientada para ser visualizada desde móviles.
Esta es la parte donde reside el código de nuestra aplicación. La aplicación en sí se implementa como una página web, por defecto un archivo local denominado index.html, que hace referencia a CSS, JavaScript, imágenes, archivos multimedia u otros recursos que son necesarios para su ejecución.
Este contenedor tiene un archivo muy importante config.xml que proporciona información sobre la aplicación y especifica parámetros que afectan a su funcionamiento.

[examples/webapp](./examples/webapp/index.html)

### WebView

Es un componente nativo presente en algunas plataformas. Es una view que muestra contenido web.

[WebView - Android](https://developer.android.com/reference/android/webkit/WebView.html)

[UIWebView - iOS](https://developer.apple.com/documentation/uikit/uiwebview)

### Plugins

Los plugins son una parte fundamental de una aplicación cordova. Estos proporcionan una interfaz para que Cordova y los componentes nativos se comuniquen entre sí. Esto nos permite invocar código nativo desde javascript.

#### Core Plugins

El proyecto Apache Cordova mantiene un conjunto de plugins denominados *Core Plugins*. Estos core plugins proporcionan a tu aplicación acceso a las funciones del dispositivo, como la batería, la camara, los contactos, etc.

[Batery-Status Plugin](https://github.com/apache/cordova-plugin-battery-status)

[Camera Plugin](https://github.com/apache/cordova-plugin-camera)

[Contacts Plugin](https://github.com/apache/cordova-plugin-contacts)

[Device Plugin](https://github.com/apache/cordova-plugin-device)

[Splashscreen Plugin](https://github.com/apache/cordova-plugin-splashscreen)

#### Third-Party Plugins

Además de los core plugins existen varios plugins de terceros que proporcionan enlaces a funciones nativas no necesariamente disponibles en todas las plataformas.

_Cuando crea un proyecto Cordova no tiene ningún plugin presente. Este es el nuevo comportamiento predeterminado. Cualquier plugin que desee, incluso los core plugins, debe agregarse de forma explícita._

[GooglePlus Plugin](https://github.com/EddyVerbruggen/cordova-plugin-googleplus)

[GoogleMaps Plugin](https://github.com/mapsplugin/cordova-plugin-googlemaps)

[DatePicker Plugin](https://github.com/VitaliiBlagodir/cordova-plugin-datepicker)

#### Custom Plugins
...

## Ejercicios

### 1. Micro-App
Imagina una micro-app para trabajar durante el curso. Esta aplicación debe ser lo más pequeña posible con un máximo de 3 pantallas y debe interactuar con componentes del dispositivo.

### 2. Webapp
Crea una pequeña página donde describas tu micro-app. Esta página debe contener recursos (estilos, scripts, imagenes, etc).

El contenido debe encontrarse en una carpeta con el nombre de la micro-app y debe ser subido al repositorio dentro de la carpeta _training/introduccion/_

### 3. Apache Cordova Plugins
Lista todos los core plugins de Apache Cordova.

### 4. Third-Party Plugins
Busca 5 plugins de terceros, comenta para que sirven y que plataformas soportan.
