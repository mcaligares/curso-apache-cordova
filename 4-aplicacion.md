# Apache Cordova Framework - Aplicacion

# Estructura

```
|-- config.xml
|-- hooks
|-- platforms
|-- plugins
|-- res
|-- www
|-- node_modules
|-- package.json
|-- package-lock.json
|-- .npmignore
|-- .gitignore
`-- .git
```

## Archivos de npm

```
|-- node_modules
|-- package.json
|-- package-lock.json
`-- .npmignore
```

### node_modules

Directorio donde se guardan las dependencias.

### package.json

Archivo de configuración de proyectos npm.

### package-lock.json

Archivo autogenerado que describe con exactitud el árbol de dependencias.

### .npmignore

Se especifica directorios o archivos a ignorar.

## Archivos de git

```
|-- .gitignore
`-- .git
```

### .gitignore

Se especifica directorios o archivos a ignorar.

### .git

Directorio de git.

## Archivos de Cordova

```
|-- config.xml
|-- hooks
|-- platforms
|-- plugins
|-- res
`-- www
```

### config.xml

Archivo que contiene toda la configuración de la aplicación.

#### widget

```
<widget id="test.cordova" version="0.0.1" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
  <...>
</widget>
```

| Atributo | Descripcion     |
| :------------- | :------------- |
| id       | package de la aplicación       |
| version | numero de version |
| android-versionCode | numero de version alternativo para android |
| android-packageName | package alternativo para android |
| ios-CFBundleIdentifier | package alternativo para ios |
| ios-CFBundleVersion | numero de version alternativo para ios |
| windows-packageVersion | numero de version alternativo para windows |
| packageName | package alternativo para windows |

#### name

Nombre de la aplicación

```
<widget ...>
   <name>MicroApp</name>
</widget>
```

#### description

Descripción de la aplicación

```
<widget ...>
   <description>Aplicacion de ejemplo para un curso.</description>
</widget>
```

#### author

Información de contacto con el autor

```
<widget ...>
   <author email="dev-team@kerbun.com" href="http://kerbun.com">
    Kerbun Dev Team
   </author>
</widget>
```

#### content

Define la pagina de inicio

```
<widget ...>
   <content src="indexTest.html"></content>
</widget>
```

#### access

Define dominios externos permitiendo la comunicación con la aplicación

```
<widget ...>
   <access origin="*"></access>
</widget>

<widget ...>
   <access origin="http://google.com"></access>
</widget>
```

#### allow-navigation

Controla que URL podrá navegar el WebView

```
<allow-navigation href="http://example.com/*" />

<allow-navigation href="*://*.example.com/*" />
```

#### allow-intent

Controla que URL podrá abrir el sistema

```
<allow-intent href="http://*/*" />
<allow-intent href="https://*/*" />
<allow-intent href="tel:*" />
<allow-intent href="sms:*" />
```

#### engine

Define la plataforma y la version

```
<engine name="browser" spec="^4.1.0" />
<engine name="ios" spec="^4.0.0" />
```

#### plugin

Define el plugin y la version

```
<plugin name="cordova-plugin-device" spec="^1.1.0" />
```

###### variable

Declara una variable/parametro del plugin

```
<plugin name="cordova-plugin-device" spec="^1.1.0">
    <variable name="MY_VARIABLE" value="my_variable_value" />
</plugin>
```

#### platform

Permite establecer preferencias de una determinada plataforma.

```
<platform name="android">
   <preference name="Fullscreen" value="true" />
</platform>
```

#### preferences

| Atributo | Descripcion |
| :------------- | :------------- |
| DisallowOverscroll | valor: true o false. Deshabilita la animación del scroll|
| Fullscreen | valor: true o false. Permite ocultar la barra de estado |
| BackgroundColor | valor: RGB. Cambia el fondo de la aplicación |
| Orientation | valor: default, landscape, portrait. Determina la orientación de la aplicación |

```
<preference name="Fullscreen" value="true" />
<preference name="Orientation" value="landscape" />
<preference name="DisallowOverscroll" value="true"/>
<preference name="BackgroundColor" value="0xff0000ff"/>
```

[Preferences](https://cordova.apache.org/docs/en/latest/config_ref/index.html#preference)

#### icons

Permite personalizar el icono de la aplicación.

```
<icon src="res/icon.png" />
```

Se puede usar un condicional por plataforma de la siguiente manera:

```
<platform name="android">
    <!-- iconos para android -->
    <icon src="res/android/ldpi.png" density="ldpi" />
</platform>
<platform name="ios">
    <!-- iconos para ios -->
    <icon src="res/ios/icon-60@3x.png" width="180" height="180" />
</platform>
```

[Configuring Icons](https://cordova.apache.org/docs/en/latest/config_ref/images.html)

### Hooks

Nos brinda la posibilidad de agregar scripts en los procesos de una aplicación Cordova.

| Hook           | Descripción                     |
| :------------- | :------------------------------ |
| *_platform_add | Cuando agregamos una plataforma |
| *_platform_rm  | Cuando removemos una plataforma |
| *_prepare      | En el proceso de preparación    |
| *_compile      | En el proceso de compilación    |
| *_run          | En el proceso de ejecución      |

#### Directorio

```
hooks/
|-- after_build
|   `-- after_build_message.groovy
`-- README.md
```

```
#!/usr/bin/env groovy

println "BUILD SUCCESS!!"
```

#### config.xml

```
<widget>
  ...
  <hook type="before_build" src="scripts/appBeforeBuild.js" />
  ...
</widget>
```

#### plugin.xml

...

[Más Info](http://cordova.apache.org/docs/en/latest/guide/appdev/hooks/index.html#introduction)

## Ejercicios

1. Crear un script para los procesos `*_platform_ls, _plugin_ls, *_prepare, *_compile, *_run, *_build, *_clean` .
2. Ejecutar comandos cordova para visualizar los cambios de los scripts.
3. Determinar en base a la siguiente configuración:

```
<?xml version='1.0' encoding='utf-8'?>
<widget id="com.curso.cordova" version="1.2.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
    <name>MicroApp</name>
    <description>Mi primer aplicación de prueba</description>
    <author email="mcaligares@gmail.com">Augusto Caligares</author>
    <content src="app.html" />
    <access origin="https://google.com" />
    <allow-intent href="http://*/*" />
    <allow-intent href="https://*/*" />
    <platform name="android">
        <allow-intent href="market:*" />
    </platform>
    <platform name="ios">
        <allow-intent href="itms:*" />
        <allow-intent href="itms-apps:*" />
    </platform>
    <engine name="browser" spec="^4.1.0" />
    <engine name="android" spec="~6.2.3" />
    <plugin name="cordova-plugin-camera" spec="^2.4.1" />
    <platform name="android">
      <plugin name="cordova-plugin-dialogs" spec="^1.3.3" />
    </platform>
    <plugin name="cordova-plugin-whitelist" spec="^1.3.2" />
    <plugin name="cordova-plugin-customurlscheme" spec="^4.3.0">
        <variable name="URL_SCHEME" value="microapp" />
    </plugin>
</widget>
```
- Nombre de la aplicación
- Plataformas que soporta
- Plugins que utiliza
- Archivo principal index.html
- Podremos abrir aplicaciónes externas?
