# Apache Cordova Framework - AngularJS

## AngularJS
AngularJS le permite escribir aplicaciones web del lado del cliente como si tuviera un navegador más inteligente. Te permite usar HTML antiguo (o HAML, Jade / Pug entre otros!) como plantilla y te permite extender la sintaxis HTML para expresar los componentes de tu aplicación de forma clara y sucinta. Sincroniza automáticamente los datos de la UI (vista) con los objetos JavaScript (modelo) mediante enlace de datos bidireccional. Para ayudarle a estructurar mejor su aplicación y hacerla fácil de probar, AngularJS enseña al navegador cómo hacer la inyección de dependencia y la inversión de control.

También ayuda con la comunicación del lado del servidor, domina los retornos de llamada asíncronos con promesas y objetos diferidos, y hace la navegación del lado del cliente y la vinculación profunda con hashbang urls o HTML5 pushState. ¿Y lo mejor de todo? ¡Hace que el desarrollo sea divertido!

[AngularJS Sitio Oficial](https://angularjs.org)

## (Des) Configuración (Desarrollo)

Para realizar las pruebas vamos a eliminar el tag de CSP (Content Security Policy) ubicado en el header.

```
<meta http-equiv="Content-Security-Policy" content="default-src ...">
```

## Instalación

### 1. Agregamos la librería de Angular.

```
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
```

### 2. Agregamos un atributo `ng-controller` en el DOM y un una variable message con mustache syntax

```
<div class="app" ng-controller="cordovaCtr">
  <h1>Apache Cordova {{message}}</h1>
</div>
```
### 3. Agregamos una función para inicializar el modulo de Angular

```
<script type="text/javascript">
function initAngular() {
  function cordovaCtr($scope) {
    $scope.message = ' + Angular JS';
  }
  angular.module('cordova', [])
      .controller('cordovaCtr', ['$scope', cordovaCtr]);
  angular.bootstrap(document, ['cordova']);
}
</script>
```

### 4. Agregamos la función en el evento deviceready

```
document.addEventListener("deviceready", initAngular, false);
```

### 5. Corremos la app

```
cordova run browser
```

## Versionado

### 1. Instalar Anular con npm

```
npm i angular --save
```

### 2. Agregar script para copiar los recursos

Linux
```
{
  "scripts": {
    "copy-angular-dev": "cp ./node_modules/angular/angular.js ./www/js/",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```

Windows
```
{
  "scripts": {
    "copy-angular-dev": "copy .\\node_modules\\angular\\angular.js .\\www\\js\\",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
}
```

### 3. Reemplazar referencia a librería

```
<script type="text/javascript" src="js/angular.js"></script>
```

### 4. Corremos el script

```
npm run copy-angular-dev
```

### 5. Corremos la app

## Código

### 1. Agregamos 3 views

```
// www/js/index.js
var viewApache = {
  title: 'Apache Cordova Framework'
};
var viewAngular = {
  title: 'Angular.js Framework'
};
var viewMobile= {
  title: 'Mobile Development'
};

function cordovaCtr($scope) {
  $scope.viewApache = viewApache;
  $scope.viewAngular = viewAngular;
  $scope.viewMobile = viewMobile;
}
```

```
<!-- www/index.html -->
<div class="app" ng-controller="cordovaCtr">
  <div>
    <h1>{{viewApache.title}}</h1>
  </div>
  <div>
    <h1>{{viewAngular.title}}</h1>
  </div>
  <div>
    <h1>{{viewMobile.title}}</h1>
  </div>
  <div id="deviceready" class="blink">
    <p class="event listening">Connecting to Device</p>
    <p class="event received">Device is Ready</p>
  </div>
</div>
```

### 2. Agregamos view actual

```
// www/js/index.js
function cordovaCtr($scope) {
  $scope.currentView = viewApache;

  $scope.viewApache = viewApache;
  $scope.viewAngular = viewAngular;
  $scope.viewMobile = viewMobile;
}
```

```
<!-- www/index.html -->
<div class="app" ng-controller="cordovaCtr">
  <div ng-show="currentView == viewApache">
    <h1>{{viewApache.title}}</h1>
  </div>
  <div ng-show="currentView == viewAngular">
    <h1>{{viewAngular.title}}</h1>
  </div>
  <div ng-show="currentView == viewMobile">
    <h1>{{viewMobile.title}}</h1>
  </div>
  <div id="deviceready" class="blink">
    <p class="event listening">Connecting to Device</p>
    <p class="event received">Device is Ready</p>
  </div>
</div>
```

### 3. Agregamos botones y eventos

```
// www/js/index.js
function cordovaCtr($scope) {
  ...
  $scope.nextView = function() {
    switch ($scope.currentView) {
      case viewApache: $scope.currentView = viewAngular; break;
      case viewAngular: $scope.currentView = viewMobile; break;
    }
    console.log('next view', $scope.currentView.title);
  };
  $scope.prevView = function() {
    switch ($scope.currentView) {
      case viewAngular: $scope.currentView = viewApache; break;
      case viewMobile: $scope.currentView = viewAngular; break;
    }
    console.log('prev view', $scope.currentView.title);
  };
}
```

```
<!-- www/index.html -->
<div class="app" ng-controller="cordovaCtr">
  <div ng-show="currentView == viewApache">
    <h1>{{currentView.title}}</h1>
    <button ng-click="nextView()">Next</button>
  </div>
  <div ng-show="currentView == viewAngular">
    <h1>{{currentView.title}}</h1>
    <button ng-click="prevView()">Prev</button>
    <button ng-click="nextView()">Next</button>
  </div>
  <div ng-show="currentView == viewMobile">
    <h1>{{currentView.title}}</h1>
    <button ng-click="prevView()">Prev</button>
  </div>
  <div id="deviceready" class="blink">
    <p class="event listening">Connecting to Device</p>
    <p class="event received">Device is Ready</p>
  </div>
</div>
```

### 4. Agregamos estados en el historial

```
var viewApache = {
  name: 'apache',
  title: 'Apache Cordova Framework'
};
var viewAngular = {
  name: 'angular',
  title: 'Angular.js Framework'
};
var viewMobile= {
  name: 'mobile',
  title: 'Mobile Development'
};
var nextView = function(view) {
  switch (view) {
    case viewApache: this.currentView = viewAngular; break;
    case viewAngular: this.currentView = viewMobile; break;
  }
  console.log('next view', this.currentView.title);
  history.pushState({}, this.currentView.title, this.currentView.name);
};
var prevView = function(view) {
  switch (view) {
    case viewAngular: this.currentView = viewApache; break;
    case viewMobile: this.currentView = viewAngular; break;
  }
  console.log('prev view', this.currentView.title);
  history.replaceState({}, this.currentView.title, this.currentView.name);
}
```

## Routing

### 1. Instalamos Angular-Route con npm

```
npm i angular-route --save
```

### 2. Agregamos el script para copiar el recurso

Linux
```
{
  "scripts": {
    ...
    "copy-angular-route": "cp ./node_modules/angular-route/angular-route.js ./www/js/",
    ...
  }
}
```
Windows

```
{
  "scripts": {
    ...
    "copy-angular-route": "copy .\\node_modules\\angular-route\\angular-route.js .\\www\\js\\",
    ...
  }
}
```

### 3. Corremos el script

```
npm run copy-angular-route
```

### 3. Agregamos la librería de Angular-Router

```
<script src="js/angular-route.js"></script>
```
### 4. Cambiamos el contenido html

```
<!-- www/index.html -->
<div class="app">
  <a href="#!apache">Apache</a>
  <a href="#!angular">Angular</a>
  <a href="#!mobile">Mobile</a>
  <ng-view></ng-view>
  <div id="deviceready" class="blink">
    <p class="event listening">Connecting to Device</p>
    <p class="event received">Device is Ready</p>
  </div>
</div>
```

### 5. Agregamos el template para los contenidos

```
<!-- www/template.html -->
<div>
  <h1>{{title}}</h1>
</div>
```

### 6. Creamos los componentes para los templates

```
var app = angular.module('cordova', ["ngRoute"]);
app.controller('apacheCtr', ['$scope', function($scope) {
  $scope.title = "Apache Cordova Framework";
}]);
app.controller('angularCtr', ['$scope', function($scope) {
  $scope.title = "AngularJS Framework";
}]);
app.controller('mobileCtr', ['$scope', function($scope) {
  $scope.title = "Mobile Development";
}]);
```

### 7. Definimos el router

```
app.config(function($routeProvider) {
  $routeProvider
    .when("/apache", {
      controller : "apacheCtr",
      templateUrl: 'template.html'
    })
    .when("/angular", {
      controller : "angularCtr",
      templateUrl: 'template.html'
    })
    .when("/mobile", {
      controller : "mobileCtr",
      templateUrl: 'template.html'
    });
});
```

### 8. Corremos la app
