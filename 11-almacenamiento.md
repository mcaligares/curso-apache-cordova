# Apache Cordova Framework - Almacenamiento

## Almacenamiento

Las API de almacenamiento web proporciona mecanismos mediantes los cuales los navegadores pueden almacenar pares key/value de una manera mucho más intuitiva que el uso de cookies.

[Storage Data](https://cordova.apache.org/docs/en/latest/cordova/storage/storage.html)

### SessionStorage

Mantiene un área de almacenamiento separada para cada origen dado que está disponible durante la sesión de la página (siempre que el navegador esté abierto, incluidas las recargas y restauraciones de la página)

### LocalStorage
Hace lo mismo, pero persiste incluso cuando el navegador se cierra y se vuelve a abrir.

#### Desventajas

* Solo guarda _Strings_. Datos complejos deben ser serializados.
* Baja performance con grandes cantidades de datos o complejos (debido a la necesidad de serializar / des-serializar).
* Baja performance debido a la falta de indexación (significa que las búsquedas requieren iterar manualmente todos los datos).
* Baja performance debido a la API síncrona (significa que las llamadas bloquearán la interfaz del usuario).
* Cantidad total de almacenamiento limitada (normalmente alrededor de 5 MB).
* iOS almacena los datos de almacenamiento local en una ubicación que puede limpiar el sistema operativo cuando se requiere espacio.

### Storage Object

#### Properties

##### Storage.length (Read only)

Devuelve un entero representando el numero de items guardados en el objeto.

#### Methods

##### Storage.key(n)

Cuando se le pasa un número n, éste método retorna el nombre de la enésima clave en el almacenamiento.

###### Parámetro

_**n:** Un número entero que representa el número de la clave de la que desea obtener el nombre. Este es un índice basado en cero._

###### Retorna

_DOMString/String_

##### Storage.getItem(keyName)

Cuando se le pasa un nombre de clave, retorna el valor de esa clave.

###### Parámetro

_**keyName:** Una DOMString que contiene el nombre de la clave cuyo valor se quiere obtener._

###### Retorna

_Una DOMString que contiene el valor de la clave. Si la clave no existe, devuelve null._

##### Storage.setItem()

Cuando se le pasa un nombre de clave y un valor, añade dicha clave al almacenamiento, o la actualiza el valor de la clave si ya existe.

###### Parámetros

_**keyName:** Un DOMString conteniendo la clave que se quiere crear/actualizar._

_**keyValue:** Un DOMString conteniendo el valor que se le quiere dar a la clave que se está creando/actualizando._

###### Retorna
_No devuelve valor._

##### Storage.removeItem()

Cuando se le pasa el nombre de una clave, eliminará dicha clave del almacenamiento.

###### Parámetros

_**keyName: ** Una DOMString que contiene el nombre de la clave que se desea eliminar._

###### Retorna

_No devuelve valor._

##### Storage.clear()

Cuando es invocado vacía todas las claves del almacenamiento.

###### Parámetros
No recibe parámetros.

###### Retorna
No devuelve ningún valor.

### Interacción con Objetos

| Entrada | Método         | Salida |
| :------ | :------------- | :----- |
| String  | JSON.parse     | Object |
| Object  | JSON.stringify | String |

[JSON Object](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/JSON)

### Ejercicio

Hacer una pequeña aplicación que contenga dos contadores cada uno con su botón y label separados por un panel. Un contador sería para _localStorage_ y otro para _sessionStorage_. Cada vez que se presione sobre el botón +1 el contador aumentaría.

<table>
  <tr>
    <td colspan="2">LocalStorage</td>
  </tr>
  <tr>
    <td><button>+1</button></td>
    <td>contador: 0</td>
  </tr>
  <tr>
    <td colspan="2">SessionStorage</td>
  </tr>
  <tr>
    <td><button>+1</button></td>
    <td>contador: 0</td>
  </tr>
</table>
