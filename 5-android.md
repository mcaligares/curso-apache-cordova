# Apache Cordova Framework - Android

## Cordova Android

**cordova-android** es una librería de aplicaciones Android que permite la construcción de proyectos basados en Córdoba para la Plataforma Android.

Para obtener la version de **cordova-android** que tenemos en nuestro proyecto, ejecutamos:

```
cordova platform ls
```

La salida puede variar dependiendo si tenemos instalada la plataforma android en nuestro proyecto:

```
# si la tenemos instalada
Installed platforms:
  android 6.2.3

# si no la tenemos instalada
Available platforms:
  android ~6.2.2

```

[Cordova Android GitHub Project](https://github.com/apache/cordova-android)

## Soporte

**cordova-android** soporta un conjunto de Andorid API-Levels

| cordova-android | Android API |
| :-------------- | :---------- |
| 6.X.X           | 16 - 25     |
| 5.X.X           | 14 - 23     |
| 4.1.X           | 14 - 22     |
| 4.0.X           | 10 - 22     |
| 3.7.X           | 10 - 21     |

[Versiones de la plataforma](https://developer.android.com/about/dashboards/index.html)

## Requisitos

Para conocer los requisitos que necesitamos para correr **cordova-android** podemos ejecutar el siguiente comando:

```
cordova requirements android
```

El resultado puede variar dependiendo si tenemos o no algunos requisitos cumplidos

```
Requirements check results for android:
Java JDK: installed 1.8.0
Android SDK: installed true
Android target: installed android-25,android-22,android-21
Gradle: installed /opt/gradle/gradle-2.4/bin/gradle
```

## Java JDK

### Descarga

Descarga disponible para varias plataformas: [Java SE Development Kit 8 Downloads](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Lista de comprobación checksum: [Checksum List](https://www.oracle.com/webfolder/s/digest/8u144checksum.html)

### Instalación

#### Windows

1. Descargar
2. Ejecutar el .exe
3. Next, Next, Next

**Video - Instalar JDK en Windows**

[![Video - Instalar JDK en Windows](http://img.youtube.com/vi/j0a_a70MXCM/0.jpg)](http://www.youtube.com/watch?v=j0a_a70MXCM)

**Video - Variables de entorno JAVA en Windows**

[![Video - Variables de entorno JAVA en Windows](http://img.youtube.com/vi/8oFZ06yGsnM/0.jpg)](http://www.youtube.com/watch?v=8oFZ06yGsnM)

#### Mac OS

1. Descargar
2. Ejecutar el .dmg
3. Next, Next, Next

#### GNU Linux

1. download jdk
2. unpack
```
sudo tar xfv jdk*.tar.gz
```
3. install alternative
```
sudo update-alternative --install /opt/java/jdk jdk /opt/java/jdk1.8.0_40 18040
```

4. add JAVA_HOME to PATH
```
export JAVA_HOME=/opt/java/jdk
export PATH=$JAVA_HOME/bin:$PATH
```
5. add permanently JAVA_HOME to path
```
printf '\nexport JAVA_HOME=/opt/java/jdk\nexport PATH=$PATH:/opt/java/jdk/bin\n' >> ~/.bashrc
```

**Video - Como instalar Oracle JDK en GNU Linux**

[![Video - Como instalar Oracle JDK en GNU Linux](http://img.youtube.com/vi/IKOdS-O62dw/0.jpg)](http://www.youtube.com/watch?v=IKOdS-O62dw)

## Gradle

[Descarga](https://gradle.org/releases/)

[Instalación](https://gradle.org/install/)

1. download zip
2. unzip
```
unzip gradle-*.zip
```

3. install alternative
```
sudo update-alternative --install /opt/gradle/current gradle /opt/gradle/gradle-2.4 24
```

4. add GRADLE_HOME to PATH
```
export GRADLE_HOME=/opt/gradle/current
export PATH=$GRADLE_HOME/bin:$PATH
```
5. add permanently GRADLE_HOME to path
```
printf '\nexport GRADLE_HOME=/opt/gradle/current\nexport PATH=$PATH:/opt/gradle/current/bin\n' >> ~/.bashrc
```

[![Video - Como instalar Gradle en Windows](http://img.youtube.com/vi/y-7wiwfcdfg/0.jpg)](http://www.youtube.com/watch?v=y-7wiwfcdfg)

## Android SDK

### Descarga
| Plataforma | SDK Tool | Tamaño | Hash |
| :--------- | :------- | :----- | :--- |
| Windows | [sdk-tools-windows-3859397.zip](https://dl.google.com/android/repository/sdk-tools-windows-3859397.zip) |132 MB |7f6037d3a7d6789b4fdc06ee7af041e071e9860c51f66f7a4eb5913df9871fd2|
|Linux|[sdk-tools-linux-3859397.zip](https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip)|130 MB|444e22ce8ca0f67353bda4b85175ed3731cae3ffa695ca18119cbacef1c1bea0|

### Instalación

#### Windows
Next, next, next

[![Video - Instalar Android SDK en Windows](http://img.youtube.com/vi/URzvWfYr20c/0.jpg)](http://www.youtube.com/watch?v=URzvWfYr20c)

#### GNU Linux

1. create ANDROID_HOME directory
```
mkdir /opt/android-sdk-linux
```
2. export ANDROID_HOME variable
```
# export variable
export ANDROID_HOME=/opt/android-sdk-linux
# add permanently ANDROID_HOME variable
printf '\nexport ANDROID_HOME=/opt/android-sdk-linux\n' >> ~/.bashrc
```
3. download sdk
```
wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
```
4. unzip on ANDROID_HOME directory
```
unzip sdk-tools-linux-3859397.zip
```
5. install tools, platform-tools, sdk platform for android-25, latest build-tools and Andorid Support Repository
```
$ANDROID_HOME/tools/bin/sdkmanager "tools" "platforms;android-25" "build-tools;26.0.1" "extras;android;m2repository" --sdk_root=$ANDROID_HOME
```
6. add ANDROID_HOME to PATH
```
# export variable
export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH
# add permanently
printf '\nexport PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH\n' >> ~/.bashrc
```

## Configurar el entorno

### Android Target

Abrimos el Android SDK Manager con el comando `android` o `sdkmanager` e instalamos los siguientes paquetes:

- Android SDK build-tools
- Android Platform SDK
- Android Support Repository

[¿Qué es el nivel de API?](https://developer.android.com/guide/topics/manifest/uses-sdk-element.html#ApiLevels)

### Android Debug Bridge - ADB

Android Debug Bridge (ADB) es una herramienta de líneas de comandos versátil que te permite comunicarte con una instancia de un emulador o un dispositivo Android conectado. Esta herramienta proporciona diferentes acciones en dispositivos, como la instalación y la depuración de apps, y proporciona acceso a un shell Unix que puedes usar para ejecutar varios comandos en un emulador o un dispositivo conectado.

```
adb version
```

[Android Debug Bridge](https://developer.android.com/studio/command-line/adb.html)

[Command Line Tools](https://developer.android.com/studio/command-line/index.html)

### Habilitar la depuración de ADB en tu dispositivo

Para usar ADB con un dispositivo conectado a través de USB, debes habilitar USB debugging en la configuración del sistema del dispositivo, que se encuentra en **Developer options**.

En Android 4.2 y versiones posteriores, la pantalla Developer options se encuentra oculta de forma predeterminada. Para poder visualizarla, dirígete a **Settings > About phone** y presiona **Build number** siete veces. Cuando regreses a la pantalla anterior, verás **Developer options** en la parte inferior.

En algunos dispositivos, la pantalla **Developer options** puede encontrarse en otro lugar o tener un nombre diferente.

_**Nota:** Cuando conectas un dispositivo con Android 4.2.2 o una versión posterior, en el sistema se muestra un diálogo en el que se solicita aceptar una clave RSA para realizar la depuración a través de esta computadora. Este mecanismo de seguridad protege los dispositivos del usuario porque garantiza que la depuración a través de USB y otros comandos de ADB no puedan ejecutarse a menos que puedas desbloquear el dispositivo y aceptar el contenido del diálogo._

### Conectar tu dispositivo por USB

Una vez conectado el dispositivo y aceptado el diálogo de aceptar clave RSA, podemos ver si nuestro sistema detecta el dispositivo con el comando:
```
adb devices
```
[Configuración de un dispositivo para desarrollo](https://developer.android.com/studio/run/device.html#setting-up)

## Ejercicio

1. En grupo repasemos los pasos para configurar el entorno hasta llegar a listar los dispositivos conectados.
2. Crear una aplicación cordova (+ proyecto npm) llamada **cordova-android** y agregamos la plataforma **android**.
3. Cambiar el contenido del archivo _www/index.html_ y correr la aplicación en un dispositivo.
4. Agregar un script en el _package.json_ para correr la aplicación en un dispositivo y otra para correr en el navegador.
