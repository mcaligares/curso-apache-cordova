# Apache Cordova Framework - Requisitos

## GIT
Sitema de control de versiones distribuido.


[Acerca del Control de Versiones](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

### Instalación

[Instalación de GIT](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git)

#### GNU Linux

Debian/Ubuntu

```
# apt-get install git
```

Fedora

```
# yum install git (up to Fedora 21)
# dnf install git (Fedora 22 and later)
```

Gentoo

```
# emerge --ask --verbose dev-vcs/git
```

Arch Linux

```
# pacman -S git
```

openSUSE

```
# zypper install git
```

Mageia
```
# urpmi git
```

FreeBSD

```
# pkg install git
```

Solaris 9/10/11 (OpenCSW)

```
# pkgutil -i git
```

Solaris 11 Express

```
# pkg install developer/versioning/git
```

OpenBSD

```
# pkg_add git
```
Alpine

```
$ apk add git
```
*PRO*

[GIT Tarball](https://www.kernel.org/pub/software/scm/git/)

#### Windows
[https://git-scm.com/download/win](https://git-scm.com/download/win)

Next, next, next...

#### MAC OS
[https://git-scm.com/download/mac](https://git-scm.com/download/mac)

Next, next, next...

### GUI Clients

[https://git-scm.com/downloads/guis](https://git-scm.com/downloads/guis)

## Node.js
Node.js® es un entorno de ejecución para JavaScript construido con el motor de JavaScript V8 de Chrome.
El ecosistema de paquetes de Node.js, npm, es el ecosistema mas grande de librerías de código abierto en el mundo.

[Chrome V8](https://developers.google.com/v8/)

[NPM Package Manager](https://www.npmjs.com/)

### Descarga

[Descargas](https://nodejs.org/en/download/)

### Instalación

#### GNU Linux

Arch Linux

```
pacman -S nodejs npm
```

Debian/Ubuntu
```
#sudo apt-get install -y build-essential

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

[Instalar desde el package manager](https://nodejs.org/en/download/package-manager/)

#### Windows

Next, next, next...

#### MAC OS

Next, next, next...

### Estructura

```
/opt/nodejs/current/
|-- bin
|   |-- bower -> ../lib/node_modules/bower/bin/bower
|   |-- cordova -> ../lib/node_modules/cordova/bin/cordova
|   |-- gulp -> ../lib/node_modules/gulp/bin/gulp.js
|   |-- ionic -> ../lib/node_modules/ionic/bin/ionic
|   |-- node
|   `-- npm -> ../lib/node_modules/npm/bin/npm-cli.js
|-- CHANGELOG.md
|-- etc
|-- include
|   `-- node
|-- lib
|   `-- node_modules
|-- LICENSE
|-- README.md
`-- share
    |-- doc
    |-- man
    `-- systemtap

10 directories, 9 files
```

### Comandos

#### npm init

```
# crear archivo package.json de manera interactiva
npm init
# crear archivo package.json sin preguntar
npm init -f
```

[Comando npm init](https://docs.npmjs.com/cli/init)

#### npm start

```
# ejecuta el script start dentro del package.json
npm start
```

[Comando npm start](https://docs.npmjs.com/cli/start)

#### npm install

```
# instala un paquete a nivel local
npm install paquete

# instala un paquete a nivel local y guarda la configuración en el package.js
npm install paquete --save

# instala un paquete a nivel global (necesita privilegios según la instalación de node)
npm install paquete -g
```

[Comando npm install](https://docs.npmjs.com/cli/install)

### Scripts

Los proyectos creados con npm nos permiten definir scripts para automatizar tareas. Se definen dentro de `scripts` como objetos `"NOMBRE_SCRIPT":"SCRIPT"`.
Ejemplo:
```
{
    ...
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    ...
}
```
En este ejemplo el script se denomina **test** y al ser ejecutado imprimirá por consola el mensaje _"Error:no test specified"_. El operador lógico _&&_ es equivalente a _Y_, es decir sólo se ejecutará el comando siguiente si el anterior devuelve `0` o `true`. El siguiente comando `exit 1` lo que hará será terminar la ejecución del script con un error, por lo tanto se mostrará por consola un stacktrace.

#### Agregar un nuevo script

Para agregar un script sólo hace falta agregarlo a la lista de `scripts`.

```
{
    ...
    "scripts": {
        "miscript": "echo \"Mi script personalizado!\"",
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    ...
}
```

#### Ejecutar un script

Para ejecutar un script debemos utilizar el comando `run` de npm.

```
npm run nombre_del_comando
```

En el caso de un script denominado _start_ no será necesario el comando `run` ya que con el comando `start` buscará el script cuyo nombre sea _start_ y lo ejecutará.

## Instalación de Apache Cordova

Para instalar el framework Apache Cordova debemos ejecutar el siguiente comando:

```
npm i -g cordova
```

Este comando nos instalará la última version de cordova de manera global en nuestro entorno.
Luego podemos ejecutar el comando `cordova -v` que nos devolverá el numero de version que tenemos instalado.

## Ejercicios

### 1. Instalar GIT y Node.js
Instalar los programas necesarios para el curso.

### 2. Crear cuenta en GitLab o GitHub
Anotar los email de los alumnos para agregarlos como colaboradores del repositorio.

### 3. Crear un proyecto aplicando lo aprendido

- Crear una pagina html con el nombre de cada uno centrado en la pantalla.
- Crear el archivo package.js.
- Agregar un start script para abrir el browser y ver la pagina anteriormente creada.
- Instalar un npm package y guardarlo como dependencia.
- Crear un repositorio git.
- Ignorar los archivos necesarios.
- Subir el proyecto a un repositorio.
