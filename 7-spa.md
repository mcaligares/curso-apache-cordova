# Apache Cordova Framework - SPA

## Single Page Application

Un single-page application (SPA), o aplicación de página única es una aplicación web o es un sitio web que cabe en una sola página con el propósito de dar una experiencia más fluida a los usuarios como una aplicación de escritorio. En un SPA todos los códigos de HTML, JavaScript, y CSS se carga de una vez1 o los recursos necesarios se cargan dinámicamente como lo requiera la página y se van agregando, normalmente como respuesta de las acciones del usuario. La página no tiene que cargar otra vez en ningún punto del proceso tampoco se transfiere a otra página, aunque las tecnologías modernas (como el pushState() API del HTML5) pueden permitir la navegabilidad en páginas lógicas dentro de la aplicación. La interacción con las aplicaciones de página única pueden involucrar comunicaciones dinámicas con el servidor web que está detrás.

[Wikipedia SPA](https://es.wikipedia.org/wiki/Single-page_application)

## SPA de Ejemplo

### 0. (Des) Configuramos CSP

Para realizar las pruebas vamos a eliminar el tag de CSP (Content Security Policy) ubicado en el header.

```
<meta http-equiv="Content-Security-Policy" content="default-src ...">
```

### 1. Creamos un proyecto cordova

```
cordova create cordova-spa
cd cordova-spa
cordova platform add browser --save
```

### 2. Agregamos contenido de views

```
<body>
  ...
  <div id="apache">
    <h2>Apache Cordova Framework</h2>
    <button data-to="js">Next</button>
  </div>
  <div id="js">
    <h2>Javascript</h2>
    <button data-to="apache">Prev</button>
    <button data-to="css">Next</button>
  </div>
  <div id="css">
    <h2>Cascading Style Sheets</h2>
    <button data-to="js">Prev</button>
  </div>
  ...
</body>
```

### 3. Agregamos un poco de codigo

```
<body>
  ...
  <script type="text/javascript">
    var currentView = 'apache';

    function goto(id) {
      currentView = id;
      var views = document.querySelectorAll('#apache, #js, #css');
      views.forEach(function(view) {
        view.style.display = view.id === id ? 'block' : 'none';
      });
    }

    var buttons = document.querySelectorAll('button');
    buttons.forEach(function(button) {
      button.onclick = function() {
        goto(button.dataset.to);
      };
    });

    goto(currentView);
  </script>
</body>
```

### 4. Corremos el proyecto

```
cordova run browser
```
