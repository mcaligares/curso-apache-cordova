# Apache Cordova Framework - Comandos

## Comandos Globales

### create

Crear un proyecto cordova

```
cordova create carpeta package nombre --template template.git
```

### help

Obtener ayuda de un comando

```
cordova help comando
cordova comando -h
cordova -h comando
```

### telemetry

Habilitar o Deshabilitar telemetry

```
cordova telemetry on
cordova telemetry off
```

## Comandos Locales o de Proyecto

### info

Obtiene información del proyecto

```
cordova info
```

### requirements

Obtiene información de los requerimientos de una plataforma

```
cordova requirements
```

### platform

Realiza la gestión de plataformas

```
cordova platform
cordova platforms
```

```
cordova platform add platform
cordova platform add platform --save
cordova platform add platform --fetch
```

```
cordova platform rm platform
cordova platform remove platform
cordova platform remove platform --save
cordova platform remove platform --fetch
```

```
cordova platform update platform
cordova platform update platform
cordova platform update platform --save
cordova platform update platform --fetch
```

```
cordova platform list
```

### plugin

Realiza la gestión de plugins

```
cordova plugin add plugin-name
cordova plugin add plugin-name --save
cordova plugin add plugin-name --fetch
```

```
cordova plugin rm plugin-name
cordova plugin remove plugin-name
cordova plugin remove plugin-name --save
cordova plugin remove plugin-name --fetch
```

```
cordova plugin list
```

```
cordova plugin search keyword
```

### prepare

Realiza la preparación de la/s plataforma/s. Lee todas las configuraciones guardadas del config.xml y las prepara para la/s plataforma/s.

```
cordova prepare
cordova prepare platform
```

### compile

Realiza la compilación de la/s plataforma/s, únicamente la compilación sin pasar por la preparación.

```
cordova compile
cordova compile platform
```

### clean

Limpia todos los archivos compilados de la/s plataforma/s.

```
cordova clean
cordova clean platform
```

### build

Realiza la preparación y compilación de la/s plataforma/s.

```
cordova build
cordova build platform
```

### run

Prepara, compila y deploya la aplicación de una plataforma específica a un device o emulador.

```
cordova run platform
cordova run platform --target XXXXX
cordova run platform --devices XXXXX
cordova run platform --emulator XXXXX
```

### serve

```
cordova serve
```

## Ejercicios

1. Agregar la plataforma browser y 2 plugins de terceros (guardar la configuración).
2. Borrar el contenido de las carpetas platforms y plugins.
3. Correr el comando `cordova prepare` y consultar el contenido de las carpetas platforms y plugins.
4. Agregar al script _start_ el comando para preparar el proyecto cordova y luego correr la aplicación en el browser.
5. Subir el proyecto cordova a un repositorio, teniendo en cuenta las configuraciones de un proyecto npm.
